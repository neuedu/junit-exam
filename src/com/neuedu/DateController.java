package com.neuedu;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateController {

    /**
     * 根据考试科目开始时间和结束时间，时间精确到秒，需要通过程序判断当前竞赛的状态，
     * 若当前系统时间在开始时间之前输出“未开始”，若当前系统时间在开始时间到结束时间之间输出“进行中”，若当前系统时间超过结束时间输出“已结束”。
     *
     * @param startTime 开始时间
     * @param endTime   结束时间
     */
    public String dateComparison(String startTime, String endTime) {
        LocalDateTime start = LocalDateTime.parse(startTime, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        LocalDateTime end = LocalDateTime.parse(endTime, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        try {
            if (start.compareTo(end) > 1) {
                return "开始时间不能大于结束时间";
            }
            if (LocalDateTime.now().compareTo(start) <= 0) {
                return "未开始";
            } else if (LocalDateTime.now().compareTo(end) >= 0 && LocalDateTime.now().compareTo(end) <= 0) {
                return "进行中";
            } else {
                return "已结束";
            }
        } catch (Exception e) {
            return "系统异常，请联系管理员";
        }
    }

}
