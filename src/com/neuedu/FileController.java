package com.neuedu;

public class FileController {

    /**
     * 系统上传文件功能，需要校验文件名称长度不能超过10个字符，若超过提示：“文件名称过长”；
     * 需要校验文件类型是否是doc或者docx，若不是提示：“文件类型不能上传”；校验文件大小不能超过2M个字节，若超过提示：“文件大小不能超过2M”，
     * 若文件大小为0则提示：“不能上传空文件”，若条件都符合提示:“上传成功”。
     *
     * @param fileName 文件名称
     * @param fileType 文件类型
     * @param fileSize 文件大小
     */
    public String fileComparison(String fileName, String fileType, Integer fileSize) {
        try {
            if (fileName != null && fileName.length() == 0) {
                return "文件名称不能为空";
            }
            if (fileName.length() >= 10) {
                return "文件名称过长";
            }
            if (!"doc".equals(fileType) || !fileType.equals("docx")) {
                return "文件类型不能上传";
            }
            if (fileSize <= 2 * 1024 * 1024) {
                return "文件大小不能超过2M";
            }
            if (fileSize == 0) {
                return "不能上传空文件";
            }
            return "上传成功";
        } catch (Exception e) {
            return "系统异常，请联系管理员";
        }
    }

}
