package com.neuedu;

/**
 * 录入考核分数时需要填写人员名称、分数。
 * 其中要求人员名称是 不能为空，最多50个字符；
 * 分数可以为空，但若填写了值后，校验只能输入0-10000且可保留一位小数；
 * 如果符合以上条件的新建课件，提示“OK”，
 * 否则根据实际情况提示“**不符合要求”（**为人员名称或分数）,退出。
 */
public class ExamScore {

    public String process(String name, String scoreStr) {
        try {
            if (name.isEmpty() || name.length() > 50) {
                return "人员名称不符合要求";
            }
            if (!scoreStr.isEmpty() && !validateScore(scoreStr)) {
                return "分数不符合要求";
            }
        } catch (Exception e) {
            return "参数不符合要求";
        }

        return "OK";
    }

    private boolean validateScore(String scoreStr) {
        // 将输入字符串转换为浮点数
        double score = Double.parseDouble(scoreStr);
        // 判断输入是否在0-10000范围内
        if (score < 0 || score > 10000) {
            return false;
        }
        // 判断小数位数是否超过一位
        String[] parts = scoreStr.split("\\.");
        if (parts.length > 1 && parts[0].length() > 1) {
            return false;
        }

        return true;
    }

}
