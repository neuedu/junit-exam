package com.neuedu;

/**
 * 录入监考人员信息时需要填写监考证编号、状态、监考人员名称。
 * 其中要求监考证编号不可以为空，最多30个字符，只能数字和字母的组合；
 * 状态可以为空，但若有值，只允许输入0或1，其中0代表无效，1代表有效；
 * 监考人员名称可以为空，但若有值时，最多50个字符长度，
 * 如果符合以上条件的更新选手，提示“OK”，
 * 否则根据实际情况提示“**不符合要求”（**为监考证编号、状态、监考人员名称）,退出。
 */
public class Invigilator {

    public static String process(String serialNumber, String status, String organization) {
        try {
            if (null == serialNumber || serialNumber.isEmpty() || !isValidSerialNumber(serialNumber)) {
                throw new IllegalArgumentException("监考证编号不符合要求");
            }
            if ((null != status && !status.isEmpty()) && !isValidStatus(status)) {
                throw new IllegalArgumentException("状态不符合要求");
            }
            if (organization.length() >= 50) {
                throw new IllegalArgumentException("监考人员名称不符合要求");
            }
        } catch (Exception e) {
            return e.getMessage();
        }

        return "OK";
    }

    private static boolean isValidSerialNumber(String position) {
        if (position.length() > 30) {
            return false;
        }
        for (char c : position.toCharArray()) {
            if (Character.isLetterOrDigit(c)) {
                return true;
            }
        }

        return true;
    }

    private static boolean isValidStatus(String status) {
        return status.equals("0|1");
    }

}
