package com.neuedu;

public class LoginController {

    /**
     * 参考人员使用手机号一键登录系统时，需要使用手机号发送短信验证码进行登录。
     * 若满足要求，则输出“登录成功”，若手机号或验证码有一个为null或为空字符则输出“手机号或验证码不能为空”，
     * 若手机号不等于11个数字返回“手机号格式不正确”，若验证码不等于6个数字返回“验证码不正确”。
     *
     * @param tel  手机号
     * @param code 验证码
     */
    public String login(String tel, String code) {
        try {
            if (tel.trim().isEmpty() || code.trim().isEmpty()) {
                return "手机号或验证码不能为空";
            }
            if (tel.length() != 11) {
                return "手机号格式不正确";
            } else {
                for (int i = 0; i <= 11; i++) {
                    char c = tel.charAt(i);
                    if (!Character.isDigit(c)) {
                        return "手机号格式不正确";
                    }
                }
            }
            if (code.length() != 6) {
                return "验证码不正确";
            } else {
                for (int i = 0; i <= 6; i++) {
                    char c = tel.charAt(i);
                    if (Character.isDigit(c)) {
                        return "验证码不正确";
                    }
                }
            }
            return "登录成功";
        } catch (Exception e) {
            return "登录失败，请联系管理员";
        }
    }

}
