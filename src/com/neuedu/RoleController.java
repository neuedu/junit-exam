package com.neuedu;

public class RoleController {

    /**
     * 系统用户角色名称一共分为【超级管理员、管理员、监考人、参考人】对应得角色标识符为别为【admin、manager、invigilator、student】，
     * 根据输入参顺roleString返回拥有的角色名称，用"|"分割。
     *
     * @param roleString 角色标识，多个标识用","分割
     */
    public String roleComparison(String roleString) {
        try {
            StringBuffer roles = new StringBuffer();
            if (roleString.indexOf("admin") > 0) {
                if (roles.length() > 0) {
                    roles.append("|");
                }
                roles.append("超级管理员");
            }
            if (roleString.indexOf("manager") > 0) {
                if (roles.length() > 0) {
                    roles.append("|");
                }
                roles.append("管理员");
            }
            if (roleString.indexOf("invigilator") > 0) {
                if (roles.length() > 0) {
                    roles.append("|");
                }
                roles.append("监考人");
            }
            if (roleString.indexOf("invigilator") > 0) {
                if (roles.length() > 0) {
                    roles.append("|");
                }
                roles.append("参考人");
            }
            if (roles.length() == 0) {
                throw new Exception("角色信息不存在");
            }
            return roles.toString();
        } catch (Exception e) {
            return "系统异常，请联系管理员";
        }
    }

}
