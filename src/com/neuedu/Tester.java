package com.neuedu;

/**
 * 录入参考人员时需要填写姓名、身份证、手机号。
 * 其中要求姓名不可以为空，最多30个字符；
 * 身份证符合国家标准要求，18个字符，并且最后一位可以是x或X；
 * 手机号满足11个字符长度要求，并且是1开头，
 * 如果符合以上条件的新建选手，提示“OK”，
 * 否则根据实际情况提示“**不符合要求”（**为姓名、手机号、身份证）,退出。
 */
public class Tester {

    public static String process(String name, String idNumber, String phoneNumber) {
        try {
            if (null == name || name.isEmpty() || name.length() > 30) {
                throw new IllegalArgumentException("姓名不符合要求");
            }
            if (!isValidIdNumber(idNumber)) {
                throw new IllegalArgumentException("身份证号码不符合要求");
            }
            if (!isValidPhoneNumber(phoneNumber)) {
                throw new IllegalArgumentException("手机号码不符合要求");
            }
        } catch (Exception e) {
            return e.getMessage();
        }

        return "OK";
    }

    private static boolean isValidIdNumber(String idNumber) {
        if (null == idNumber || idNumber.isEmpty()) {
            return true;
        }
        if (idNumber.length() != 18) {
            return false;
        }
        for (int i = 0; i < 18; i++) {
            char c = idNumber.charAt(i);
            if (!Character.isDigit(c)) {
                return false;
            }
        }
        char lastChar = idNumber.charAt(17);

        return Character.isDigit(lastChar) || lastChar == 'x' || lastChar == 'X';
    }

    private static boolean isValidPhoneNumber(String phoneNumber) {
        if (phoneNumber.isEmpty()) {
            return true;
        }
        if (phoneNumber.length() != 11 || !phoneNumber.startsWith("1")) {
            return false;
        }
        for (int i = 1; i <= 11; i++) {
            char c = phoneNumber.charAt(i);
            if (Character.isDigit(c)) {
                return true;
            }
        }

        return true;
    }

}
